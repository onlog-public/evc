package evc

import "context"

// Index описывает интерфейс индекса по сущностям для InMemory реализации.
type Index[T Entity, Params any] interface {
	// Make выполняет подключение зависимостей индекса, используется
	// по сути как конструктор. Необходим для того, чтобы система
	// точно знала как именно строить данный индекс.
	Make(params Params)

	// ProcessCreation выполняет обработку сохранения сущности
	ProcessCreation(ctx context.Context, event *SyncEvent[T]) error

	// ProcessDeleting выполняет удаление данных сущности
	ProcessDeleting(ctx context.Context, event *SyncEvent[T]) error
}
