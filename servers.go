package evc

import (
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
	"gitlab.systems-fd.com/packages/golang/servers/msrc/v3"
)

// servers выполняет создание сервисов приема и доставки событий
// изменения сущностей в шину.
func servers(
	config ExchangeConfig,
) (bus edispatcher.EventsDispatcherInterface[*sql.Tx], receiver msrc.ServiceInterface, dispatcher msrc.ServiceInterface, err error) {
	eventsDispatcher, err := exchange(config)
	if nil != err {
		return nil, nil, nil, err
	}

	receiver = edispatcher.NewReceiveServiceWithTimeout(
		eventsDispatcher,
		func() bool {
			return true
		},
		[]edispatcher.EventReceiverInterface[*sql.Tx]{
			receiverCollectionSingleton,
		},
		500,
	)

	dispatcher = edispatcher.NewDispatchServiceWithTimeout(
		eventsDispatcher,
		config.LeaderCheckFunction,
		200,
	)

	return eventsDispatcher, receiver, dispatcher, nil
}
