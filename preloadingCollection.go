package evc

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// preloadingCollectionSingleton содержит singleton коллекции сервисов
// предварительной загрузки данных индексов
var preloadingCollectionSingleton *preloadingCollection

// preloadingCollection реализует коллекцию сервисов
// предварительной загрузки данных индексов
type preloadingCollection struct {
	preloaderCollection []preloader
	indexes             map[string][]tPreloaderFunc
}

// buildPreloadingCollection создает singleton коллекции
func buildPreloadingCollection() {
	preloadingCollectionSingleton = &preloadingCollection{
		preloaderCollection: make([]preloader, 0),
		indexes:             make(map[string][]tPreloaderFunc),
	}
}

// RegisterPreloader выполняет регистрацию сервиса предварительной
// загрузки данных индекса
func (p *preloadingCollection) RegisterPreloader(preloader preloader) {
	p.preloaderCollection = append(p.preloaderCollection, preloader)
}

// RegisterIndex выполняет регистрацию индекса для подписки на события изменения
func (p *preloadingCollection) RegisterIndex(entityName string, indexFn tPreloaderFunc) {
	if _, ok := p.indexes[entityName]; !ok {
		p.indexes[entityName] = make([]tPreloaderFunc, 0)
	}

	p.indexes[entityName] = append(p.indexes[entityName], indexFn)
}

// Preload выполняет предварительную загрузку данных сущностей
// в индекс. Необходимо использовать только один раз, при запуске
// приложения.
func (p *preloadingCollection) Preload() error {
	for _, preloader := range p.preloaderCollection {
		if err := preloader.Preload(p.processEvent); err != nil {
			return errors.Wrap(err, `preloading failed`)
		}
	}

	return nil
}

// processEvent выполняет обработку события предварительной загрузки данных индекса
func (p *preloadingCollection) processEvent(ctx context.Context, event *SyncEvent[Entity]) error {
	indexes, ok := p.indexes[h.VariableTypeName(event.Entity)]
	if !ok {
		return nil
	}

	for _, index := range indexes {
		if err := index(ctx, event); nil != err {
			return errors.Wrap(err, `indexing failed`)
		}
	}

	return nil
}
