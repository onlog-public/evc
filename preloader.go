package evc

import (
	"context"
)

// preloadingCreationFunc описывает тип функции предварительной загрузки данных
// для первоначального наполнения индекса
type preloadingCreationFunc[T Entity] func(ctx context.Context, event *SyncEvent[T]) error

// preloaderEventProcessor описывает callback обработки событий загрузки данных в индексы
type preloaderEventProcessor func(ctx context.Context, event *SyncEvent[Entity]) error

// preloader описывает интерфейс сервиса предварительной загрузки
// данных индексов
type preloader interface {
	// Preload выполняет предварительную загрузку данных сущностей
	// в индекс. Необходимо использовать только один раз, при запуске
	// приложения.
	//
	// В качестве аргумента принимает обработчик доставки событий в
	// зарегистрированные индексы.
	Preload(processEvent preloaderEventProcessor) error
}
