package evc

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// baseEvent содержит не до конца разобранные данные сущности.
// Используется для явного определения типа события и сущности.
type baseEvent struct {
	events.Event
	EntityType string
	Entity     json.RawMessage
}

// indexReceiver реализует подписчика на события шины для индекса
// определенного типа.
type indexReceiver[T Entity, Params any] struct {
	index Index[T, Params]
}

// newIndexReceiver реализует конструктор подписчика
func newIndexReceiver[T Entity, Params any](index Index[T, Params]) edispatcher.EventReceiverInterface[*sql.Tx] {
	return &indexReceiver[T, Params]{index: index}
}

// Receive выполняет получение и обработку события
func (i indexReceiver[T, Params]) Receive(
	ctx context.Context,
	eventData string,
	_ *sql.Tx,
) error {
	var baseEventData baseEvent
	err := json.Unmarshal([]byte(eventData), &baseEventData)
	if nil != err {
		return errors.Wrap(err, `[Index :: `+h.EntityName[T]()+`] failed to parse event data`)
	}

	entity := h.Create[T]()
	if entity.GetEntityType() != baseEventData.EntityType {
		return nil
	}

	err = json.Unmarshal(baseEventData.Entity, &entity)
	if nil != err {
		return errors.Wrap(err, `[Index :: `+h.EntityName[T]()+`] failed to parse event entity data`)
	}

	for _, event := range i.makeEvents(baseEventData, entity) {
		err := i.processEvent(ctx, event)
		if nil != err {
			return errors.Wrap(err, `[Index :: `+h.EntityName[T]()+`] failed to process event`)
		}
	}

	return nil
}

// processEvent выполняет обработку события для индекса.
func (i indexReceiver[T, Params]) processEvent(ctx context.Context, event SyncEvent[T]) error {
	if event.Type == events.Created {
		return i.index.ProcessCreation(ctx, &event)
	}

	return i.index.ProcessDeleting(ctx, &event)
}

// makeEvents выполняет создание событий индекса по событиям из шины
// событий. Преобразует событие репозитория в событие индекса.
func (i indexReceiver[T, Params]) makeEvents(
	baseEventData baseEvent,
	entity T,
) []SyncEvent[T] {
	var eventsToDispatch []SyncEvent[T]
	switch baseEventData.Type {
	case events.Created, events.Deleted:
		eventsToDispatch = append(eventsToDispatch, SyncEvent[T]{
			Event:      baseEventData.Event,
			EntityType: baseEventData.EntityType,
			Entity:     entity,
		})
		break
	case events.Updated:
		eventsToDispatch = append(eventsToDispatch, SyncEvent[T]{
			Event: events.Event{
				Type:            events.Deleted,
				Table:           baseEventData.Table,
				PrimaryKey:      baseEventData.PrimaryKey,
				PrimaryKeyValue: baseEventData.PrimaryKeyValue,
			},
			EntityType: baseEventData.EntityType,
			Entity:     entity,
		})
		eventsToDispatch = append(eventsToDispatch, SyncEvent[T]{
			Event: events.Event{
				Type:            events.Created,
				Table:           baseEventData.Table,
				PrimaryKey:      baseEventData.PrimaryKey,
				PrimaryKeyValue: baseEventData.PrimaryKeyValue,
			},
			EntityType: baseEventData.EntityType,
			Entity:     entity,
		})
		break
	}

	return eventsToDispatch
}
