package evc

import (
	"gitlab.com/onlog-public/base-repo"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// subscriberCollectionSingleton содержит singleton коллекции подписчиков.
var subscriberCollectionSingleton *subscriberCollection

// subscriberCollection реализует коллекцию подписчиков на события
// изменения из репозиториев. Необходимо использовать singleton
// для работы библиотеки.
type subscriberCollection struct {
	subscribers   []base_repo.Subscription
	subscriptions []events.EventsSubscriberInterface
}

// buildSubscriberCollection создает singleton коллекции
func buildSubscriberCollection() {
	subscriberCollectionSingleton = &subscriberCollection{
		subscribers: make([]base_repo.Subscription, 0),
	}
}

// IsRepositoryAlreadySubscribed возвращает флаг того, что переданный репозиторий
// уже подписан на изменения коллекции.
func (s *subscriberCollection) IsRepositoryAlreadySubscribed(subscriber base_repo.Subscription) bool {
	for _, exists := range s.subscribers {
		if h.VariableTypeName(exists) == h.VariableTypeName(subscriber) {
			return true
		}
	}

	return false
}

// Register сохраняет подписчика в коллекцию.
func (s *subscriberCollection) Register(subscriber base_repo.Subscription) {
	s.subscribers = append(s.subscribers, subscriber)
}

// Subscribe выполняет сохранение подписки в коллекцию.
func (s *subscriberCollection) Subscribe(subscription events.EventsSubscriberInterface) {
	s.subscriptions = append(s.subscriptions, subscription)
}

// Bind выполняет регистрацию подписок в подписчиках.
// Необходимо вызывать в самом конце регистрации, чтоб
// событие подписки было самым последним, т.к. доставка
// события в шину не обратима.
func (s *subscriberCollection) Bind() {
	for _, subscriber := range s.subscribers {
		for _, subscription := range s.subscriptions {
			subscriber.SubscribeToEvents(subscription)
		}
	}
}
