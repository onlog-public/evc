package evc

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"sync"
)

// BaseIDX реализует базовый индекс для работы с сущностями Entity.
type BaseIDX[T Entity] struct {
	Mx        *sync.RWMutex
	EntityMap map[string]*T
}

// MakeBaseIDX реализует базовый конструктор индекса BaseIndex
func MakeBaseIDX[T Entity]() *BaseIDX[T] {
	return &BaseIDX[T]{}
}

// GetById выполняет получение сущности по идентификатору
func (b *BaseIDX[T]) GetById(id string) *T {
	b.Mx.RLock()
	defer b.Mx.RUnlock()

	if entity, ok := b.EntityMap[id]; ok {
		return entity
	}

	return nil
}

// GetByEntityID возвращает целевую сущность по переданному идентификатору.
func (b *BaseIDX[T]) GetByEntityID(_ context.Context, id string) (T, error) {
	result := b.GetById(id)
	if nil == result {
		var empty T
		return empty, errors.New("entity not found")
	}

	return *result, nil
}

// Make выполняет подключение зависимостей индекса, используется
// по сути как конструктор. Необходим для того, чтобы система
// точно знала как именно строить данный индекс.
func (b *BaseIDX[T]) Make(globalMx *sync.RWMutex) {
	b.Mx = globalMx
	b.EntityMap = make(map[string]*T)
}

// ProcessCreation выполняет обработку сохранения сущности
func (b *BaseIDX[T]) ProcessCreation(_ context.Context, event *SyncEvent[T]) error {
	b.Mx.Lock()
	defer b.Mx.Unlock()

	if `0` == event.Entity.GetPrimaryKey() {
		return errors.New(`entity [` + h.EntityName[T]() + `] with ID is not supported`)
	}

	// Заполняем индекс локаций по ID
	b.EntityMap[event.Entity.GetPrimaryKey()] = &event.Entity

	return nil
}

// ProcessDeleting выполняет удаление данных сущности
func (b *BaseIDX[T]) ProcessDeleting(_ context.Context, event *SyncEvent[T]) error {
	b.Mx.Lock()
	defer b.Mx.Unlock()

	delete(b.EntityMap, event.PrimaryKeyValue)

	return nil
}
