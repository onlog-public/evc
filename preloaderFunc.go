package evc

import (
	"context"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// tPreloaderFunc описывает не явный загрузчик данных индекса
type tPreloaderFunc preloadingCreationFunc[Entity]

// preloaderFunc преобразовывает явную типизацию callback обработчика
// создания сущности в индексе в общую с сохранением обработки события.
// Этот callback необходим для первоначального наполнения данных индекса.
func preloaderFunc[T Entity](callback preloadingCreationFunc[T]) (string, tPreloaderFunc) {
	return h.EntityName[T](), func(ctx context.Context, event *SyncEvent[Entity]) error {
		data, ok := h.ToInterface(event.Entity).(T)
		if !ok {
			return nil
		}

		return callback(ctx, &SyncEvent[T]{
			Event:      event.Event,
			EntityType: event.EntityType,
			Entity:     data,
		})
	}
}
