package evc

import "gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"

// SyncEvent содержит данные события, отправляемого в шину для обеспечения
// согласования изменений.
type SyncEvent[T Entity] struct {
	events.Event
	EntityType string
	Entity     T
}
