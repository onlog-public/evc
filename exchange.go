package evc

import (
	"database/sql"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-helpers/v3"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-pg/v3"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-rabbitmq/v3"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
)

// ExchangeConfig описывает параметры конфигурации подключения
// к шине обмена событий для реализации функционала InMemory репозиториев
type ExchangeConfig struct {
	RabbitMqConnectionCredentials string      // Строка с параметрами подключения к RabbitMQ
	RabbitMqExchangePrefix        string      // Префикс очередей, которые будут создаваться для обмена
	DB                            *DBConfig   // Подключение к БД. Если передать, то вместо InMemory store будет использоваться БД.
	LeaderCheckFunction           func() bool // Функция проверки статуса лидера, для определения возможности доставки событий в шину.
}

// DBConfig описывает параметры подключения к базе данных для хранения событий в ней.
type DBConfig struct {
	DB              *sql.DB
	EventsTableName string
}

// exchange выполняет создание диспетчера для обмена событиями
func exchange(
	config ExchangeConfig,
) (edispatcher.EventsDispatcherInterface[*sql.Tx], error) {
	exchangeUuid, err := uuid.NewUUID()
	if nil != err {
		return nil, errors.Wrap(err, `failed to generate exchange uuid`)
	}

	exchangeName := config.RabbitMqExchangePrefix + `-` + exchangeUuid.String()

	channel, manager, err := edispatcher_rabbitmq.NewEventChannel(
		config.RabbitMqConnectionCredentials,
		edispatcher_rabbitmq.ExchangeParams{
			Name:       config.RabbitMqExchangePrefix,
			Kind:       amqp.ExchangeFanout,
			Durable:    true,
			AutoDelete: false,
			Internal:   false,
			NoWait:     false,
			Policies:   nil,
		},
		edispatcher_rabbitmq.QueueParams{
			Name:       exchangeName,
			Exclusive:  true,
			AutoDelete: false,
			Durable:    false,
			NoWait:     false,
			Policies:   nil,
		},
	)

	if nil != err {
		return nil, errors.Wrap(err, `failed to create rabbitmq channel`)
	}

	if nil != config.DB {
		return edispatcher_pg.NewPgEventsDispatcherWithTimeout(
			channel,
			manager,
			config.DB.DB,
			config.DB.EventsTableName,
			50,
			50,
			200,
		), nil
	}

	return edispatcher.NewEventsDispatcherWithTimeout[*sql.Tx](
		channel,
		edispatcher_helpers.NewInMemoryEventsStore[*sql.Tx](),
		edispatcher_helpers.NewInMemoryReceiveStore[*sql.Tx](),
		edispatcher_helpers.NewEmptyTxManager[*sql.Tx](),
		manager,
		100,
		200,
	), nil
}
