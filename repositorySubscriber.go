package evc

import (
	"context"
	"database/sql"
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
)

// repositorySubscriber реализует подписчик на изменения данных
// сущностей для доставки их в шину событий.
type repositorySubscriber[T Entity] struct {
	dispatcher edispatcher.EventsDispatcherInterface[*sql.Tx]
	repository Repository[T]
}

// newRepositorySubscriber реализует конструктор репозитория
func newRepositorySubscriber[T Entity](
	dispatcher edispatcher.EventsDispatcherInterface[*sql.Tx],
	repository Repository[T],
) events.EventsSubscriberInterface {
	return &repositorySubscriber[T]{
		dispatcher: dispatcher,
		repository: repository,
	}
}

// Process выполняет обработку события
func (r repositorySubscriber[T]) Process(
	ctx context.Context,
	event events.Event,
	tx *sql.Tx,
) error {
	entity := h.Create[T]()
	if event.Table != entity.GetEntityTable() {
		return nil
	}

	workTx := tx
	if event.Type == events.Deleted {
		workTx = nil
	}

	entityData, err := h.OneOf(r.repository.GetByID)(ctx, event.PrimaryKeyValue, workTx)
	if nil != err {
		return errors.Wrap(err, `[Subscriber[`+h.EntityName[T]()+`]]`)
	}

	if nil == entityData {
		return nil
	}

	syncEventObj := SyncEvent[T]{
		Event:      event,
		EntityType: (*entityData).GetEntityType(),
		Entity:     *entityData,
	}

	syncEventData, err := json.Marshal(syncEventObj)
	if nil != err {
		return errors.Wrap(err, `[Subscriber[`+h.EntityName[T]()+`]] failed to serialize data`)
	}

	err = r.dispatcher.Dispatch(ctx, string(syncEventData), tx)
	if nil != err {
		return errors.Wrap(err, `[Subscriber[`+h.EntityName[T]()+`]]`)
	}

	return nil
}

// Rollback выполняет откат события
func (r repositorySubscriber[T]) Rollback(_ context.Context, _ error) {}
