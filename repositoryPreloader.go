package evc

import (
	"context"
	"github.com/pkg/errors"
	"github.com/schollz/progressbar/v3"
	"github.com/sirupsen/logrus"
	"gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2/events"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"runtime"
)

// repositoryPreloader реализует сервис загрузки данных preloader.
type repositoryPreloader[T Entity] struct {
	repository Repository[T]
	logger     *logrus.Entry
}

// newRepositoryPreloader реализует конструктор сервиса
// предварительной загрузки данных индексов
func newRepositoryPreloader[T Entity](
	repository Repository[T],
) preloader {
	return &repositoryPreloader[T]{
		repository: repository,
		logger:     logrus.WithField("prefix", "evc/preloading"),
	}
}

// Preload выполняет предварительную загрузку данных сущностей
// в индекс. Необходимо использовать только один раз, при запуске
// приложения.
//
// В качестве аргумента принимает обработчик доставки событий в
// зарегистрированные индексы.
func (r *repositoryPreloader[T]) Preload(processEvent preloaderEventProcessor) error {
	ctx := context.Background()
	r.logger.Info(`Загрузка базовых данных [` + h.EntityName[T]() + `]`)

	entities, err := r.repository.GetAll(ctx, nil)
	if nil != err {
		return errors.Wrap(err, `[Preloader[`+h.EntityName[T]()+`] :: Preload] failed to load entities`)
	}

	eventsToPreload := h.Map(entities, func(entity T) *SyncEvent[Entity] {
		return &SyncEvent[Entity]{
			Event: events.Event{
				Type:            events.Created,
				Table:           entity.GetEntityTable(),
				PrimaryKey:      entity.GetPrimaryKeyColumn(),
				PrimaryKeyValue: entity.GetPrimaryKey(),
			},
			EntityType: entity.GetEntityType(),
			Entity:     entity,
		}
	})

	if 0 == len(eventsToPreload) {
		return nil
	}

	r.logger.Info(`Синхронизация индекса [` + h.EntityName[T]() + `]`)

	bar := progressbar.Default(int64(len(eventsToPreload)))
	for _, event := range eventsToPreload {
		_ = bar.Add(1)

		err := processEvent(ctx, event)
		if nil != err {
			return errors.Wrap(err, `[Preloader[`+h.EntityName[T]()+`] :: Preload] failed to dispatch preloading event`)
		}
	}

	_ = bar.Finish()

	runtime.GC()

	return nil
}
