package evc

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
	"gitlab.systems-fd.com/packages/golang/helpers/h"
	"gitlab.systems-fd.com/packages/golang/servers/msrc/v3"
)

// libraryDispatcher содержит singleton шины событий библиотеки
var libraryDispatcher edispatcher.EventsDispatcherInterface[*sql.Tx]

// isInitialized содержит глобальный флаг инициализации библиотеки.
// Используется для определения последовательности вызова методов библиотеки.
var isInitialized = false

// New выполняет инициализацию индекса для согласования данных.
// Принимает через generic тип этого индекса, выполняет его построение
// и регистрацию в системе.
func New[IDX Index[T, Params], T Entity, Params any](
	repository Repository[T],
	parameters Params,
) IDX {
	if !isInitialized {
		logrus.Fatal(`Library is not initialized. Use Initialize() first.`)
	}

	idx := h.Create[IDX]()
	idx.Make(parameters)

	// Репозиторий регистрируем только 1 раз для всех обработчиков, для
	// исключения дублирования обработки событий.
	if !subscriberCollectionSingleton.IsRepositoryAlreadySubscribed(repository) {
		subscriberCollectionSingleton.Register(repository)
		subscriberCollectionSingleton.Subscribe(newRepositorySubscriber(libraryDispatcher, repository))
		preloadingCollectionSingleton.RegisterPreloader(newRepositoryPreloader[T](repository))
	}

	receiverCollectionSingleton.Register(newIndexReceiver[T, Params](idx))
	preloadingCollectionSingleton.RegisterIndex(preloaderFunc(idx.ProcessCreation))

	return idx
}

// Initialize выполняет инициализацию сервисов библиотеки.
// Необходимо использовать перед вызовом всех прочих методов.
// Использовать необходимо только единожды.
func Initialize(config ExchangeConfig) (receiver msrc.ServiceInterface, dispatcher msrc.ServiceInterface) {
	if isInitialized {
		logrus.Fatal(`Library is initialized. Use Initialize() once.`)
	}

	buildReceiverCollection()
	buildSubscriberCollection()
	buildPreloadingCollection()

	bus, receiver, dispatcher, err := servers(config)
	if err != nil {
		logrus.WithError(err).Fatal(`Failed to initialize services for EVC.`)
	}

	libraryDispatcher = bus
	isInitialized = true

	return receiver, dispatcher
}

// PreloadIndexes выполняет заполнение индексов перед использованием.
// Необходимо вызывать единожды, при запуске приложения, но после
// инициализации.
func PreloadIndexes() error {
	if !isInitialized {
		logrus.Fatal(`Library is not initialized. Use Initialize() first.`)
	}

	subscriberCollectionSingleton.Bind()

	return preloadingCollectionSingleton.Preload()
}
