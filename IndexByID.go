package evc

import (
	"context"
	"sync"
)

// IndexByID описывает интерфейс индекса с методом получения сущности
// по переданному первичному ключу сущности.
type IndexByID[T Entity] interface {
	// GetById выполняет получение сущности по идентификатору
	GetById(id string) *T
}

// EntityIndex описывает интерфейс индекса по сущностям для системы
// расчета стоимости.
type EntityIndex[T any] interface {
	// GetByEntityID возвращает целевую сущность по переданному идентификатору.
	GetByEntityID(ctx context.Context, id string) (T, error)
}

// BaseIndex описывает интерфейс базового индекса для работы с сущностями.
type BaseIndex[T Entity] interface {
	IndexByID[T]
	EntityIndex[T]
	Index[T, *sync.RWMutex]
}
