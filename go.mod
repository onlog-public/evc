module gitlab.com/onlog-public/evc

go 1.22

require (
	github.com/google/uuid v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/schollz/progressbar/v3 v3.14.3
	github.com/sirupsen/logrus v1.9.0
	github.com/streadway/amqp v1.1.0
	gitlab.com/onlog-public/base-repo v0.0.3
	gitlab.com/onlog-public/repo-subscribers v0.0.2
	gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-helpers/v3 v3.0.1
	gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-pg/v3 v3.0.2
	gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher-rabbitmq/v3 v3.0.1
	gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3 v3.1.1
	gitlab.systems-fd.com/packages/golang/graphql/old/postgresql/pg_repo/v2 v2.4.2
	gitlab.systems-fd.com/packages/golang/helpers/h v0.23.0
	gitlab.systems-fd.com/packages/golang/servers/msrc/v3 v3.1.3
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/functionalfoundry/graphqlws v0.0.0-20200611113535-7bc58903ce7b // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/graphql-go/graphql v0.8.0 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/klauspost/compress v1.15.13 // indirect
	github.com/lib/pq v1.10.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.43.0 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2 // indirect
	gitlab.systems-fd.com/inner-packages/m v0.2.5 // indirect
	gitlab.systems-fd.com/packages/golang/graphql/old/builder/sbuilder/v2 v2.4.3 // indirect
	gitlab.systems-fd.com/packages/golang/graphql/old/helpers/gql-root-type-getter v1.3.4 // indirect
	gitlab.systems-fd.com/packages/golang/graphql/old/helpers/gql-sql-converter v1.2.7 // indirect
	gitlab.systems-fd.com/packages/golang/graphql/old/helpers/nullable v1.0.6 // indirect
	gitlab.systems-fd.com/packages/golang/helpers/sqd v1.0.2 // indirect
	golang.org/x/crypto v0.4.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/term v0.20.0 // indirect
)
