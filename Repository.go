package evc

import (
	"gitlab.com/onlog-public/base-repo"
	"gitlab.com/onlog-public/repo-subscribers"
)

// Repository описывает интерфейс репозитория, подходящего
// для реализации функционала библиотеки.
type Repository[T Entity] interface {
	base_repo.All[T]
	repo_subscribers.EntityBaseGetter[T]
	base_repo.Subscription
}
