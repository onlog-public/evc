package evc

import (
	"context"
	"database/sql"
	"gitlab.systems-fd.com/packages/golang/async-constistency/edispatcher/v3"
)

// receiverCollectionSingleton содержит singleton коллекции
var receiverCollectionSingleton *receiverCollection

// receiverCollection реализует коллекцию обработчиков
// событий изменения данных индексов. Необходимо использовать
// singleton для библиотеки.
type receiverCollection struct {
	indexReceivers []edispatcher.EventReceiverInterface[*sql.Tx]
}

// buildReceiverCollection создает singleton коллекции.
func buildReceiverCollection() {
	receiverCollectionSingleton = &receiverCollection{
		indexReceivers: make([]edispatcher.EventReceiverInterface[*sql.Tx], 0),
	}
}

// Receive выполняет получение и обработку события
func (r *receiverCollection) Receive(ctx context.Context, eventData string, tx *sql.Tx) error {
	for _, receiver := range r.indexReceivers {
		err := receiver.Receive(ctx, eventData, tx)
		if err != nil {
			return err
		}
	}

	return nil
}

// Register выполняет регистрацию подписчика в коллекции
func (r *receiverCollection) Register(receiver edispatcher.EventReceiverInterface[*sql.Tx]) {
	r.indexReceivers = append(r.indexReceivers, receiver)
}
