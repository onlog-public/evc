package evc

import (
	"gitlab.com/onlog-public/base-repo"
	"gitlab.com/onlog-public/repo-subscribers"
)

// Entity описывает сущность, подходящую для реализации функционала
// асинхронной InMemory согласованности.
type Entity interface {
	repo_subscribers.EntityWithSubscription
	base_repo.Entity
}
